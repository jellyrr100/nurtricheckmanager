package com.jay.nurtricheckmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NurtriCheckManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NurtriCheckManagerApplication.class, args);
    }

}
