package com.jay.nurtricheckmanager.entity;

import com.jay.nurtricheckmanager.enums.PillEfficacy;
import com.jay.nurtricheckmanager.interfaces.CommonModelBuilder;
import com.jay.nurtricheckmanager.model.PillRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Pill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String pillName;

    @Column(nullable = false,length = 10)
    private String pillCompany;

    @Column(nullable = false)
    private Integer pillPrice;

    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private PillEfficacy nutriInfo;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalDate expirationDate;

    @Column(nullable = false)
    private Integer totalQuantity;

    @Column(nullable = false)
    private Boolean isDaliy;

    @Column(nullable = false)
    private Integer eatTime;

    @Column(nullable = false)
    private Boolean isRepurchase;

    @Column(nullable = false)
    private Boolean isEat;

    private Pill(pillBuilder builder){
        this.pillName = builder.pillName;
        this.pillCompany = builder.pillCompany;
        this.pillPrice = builder.pillPrice;
        this.nutriInfo = builder.nutriInfo;
        this.startDate = builder.startDate;
        this.expirationDate = builder.expirationDate;
        this.totalQuantity = builder.totalQuantity;
        this.isDaliy = builder.isDaliy;
        this.eatTime = builder.eatTime;
        this.isRepurchase = builder.isRepurchase;
        this.isEat = builder.isEat;
    }
    public static class pillBuilder implements CommonModelBuilder<Pill> {
        private final String pillName;
        private final  String pillCompany;
        private final Integer pillPrice;
        private final PillEfficacy nutriInfo;
        private final LocalDate startDate;
        private final LocalDate expirationDate;
        private final Integer totalQuantity;
        private final Boolean isDaliy;
        private final Integer eatTime;
        private final Boolean isRepurchase;
        private final Boolean isEat;

        public pillBuilder(PillRequest request){
            this.pillName = request.getFillName();
            this.pillCompany = request.getFillCompany();
            this.pillPrice = request.getFillPrice();
            this.nutriInfo = request.getNutriInfo();
            this.startDate = request.getStartDate();
            this.expirationDate = request.getExpirationDate();
            this.totalQuantity = request.getTotalQuantity();
            this.isDaliy = request.getIsDaily();
            this.eatTime = request.getEatTime();
            this.isRepurchase = false;
            this.isEat = true;

        }
        @Override
        public Pill build() {
            return new Pill(this);
        }
    }
}
