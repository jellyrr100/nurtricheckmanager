package com.jay.nurtricheckmanager.entity;

import com.jay.nurtricheckmanager.interfaces.CommonModelBuilder;
import com.jay.nurtricheckmanager.model.PillDoseRequest;
import com.jay.nurtricheckmanager.model.PillRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PillDose {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pillId",nullable = false)
    private Pill pill;

    @Column(nullable = false)
    private LocalDate eatDate;

    @Column(nullable = false)
    private LocalTime eatTime;

    private String memo;

    private PillDose(PillDoseBuilder builder) {
        this.pill = builder.pill;
        this.eatDate = builder.eatDate;
        this.eatTime = builder.eatTime;
        this.memo = builder.memo;
    }

    public static class PillDoseBuilder implements CommonModelBuilder<PillDose>{
        private final Pill pill;
        private final LocalDate eatDate;
        private final LocalTime eatTime;
        private final String memo;

        public PillDoseBuilder(Pill pill, PillDoseRequest request) {
            this.pill = pill;
            this.eatDate = request.getEatDate();
            this.eatTime = LocalTime.of(request.getEatTimeHour(), request.getEatTimeMinute());
        }
        public PillDoseBuilder setMeMo(String mamo){
            this.memo = mamo;

            return this;
        }

        @Override
        public PillDose build() {
            return new PillDose(this);
        }
    }
}
