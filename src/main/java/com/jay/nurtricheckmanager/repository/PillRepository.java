package com.jay.nurtricheckmanager.repository;

import com.jay.nurtricheckmanager.entity.Pill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PillRepository extends JpaRepository<Pill, Long> {

}
