package com.jay.nurtricheckmanager.repository;

import com.jay.nurtricheckmanager.entity.PillDose;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;

public interface PillDoseRepository extends JpaRepository<PillDose,Long> {
    long countByPill_IdAndEatDate(long pillId, LocalDate eatDate);

}
