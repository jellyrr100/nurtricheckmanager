package com.jay.nurtricheckmanager.model;

import com.jay.nurtricheckmanager.enums.PillEfficacy;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class PillRequest {

    @NotNull
    @Length(min = 1,max =35)
    private String FillName;

    @NotNull
    @Length(min =1,max = 20)
    private String FillCompany;

    @NotNull
    @Min(value = 0)
    private  Integer FillPrice;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PillEfficacy nutriInfo;
    @NotNull
    private LocalDate startDate;
    @NotNull
    private LocalDate expirationDate;
    @NotNull
    @Min(value = 1)
    private Integer totalQuantity;
    @NotNull
    private Boolean isDaily;

    @NotNull
    private Integer eatTime;
}
