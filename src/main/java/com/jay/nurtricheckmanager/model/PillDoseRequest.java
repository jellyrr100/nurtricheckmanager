package com.jay.nurtricheckmanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class PillDoseRequest {
    @NotNull
    private LocalDate eatDate;

    @NotNull
    @Min(0)
    @Max(23)
    private Integer eatTimeHour;

    @NotNull
    @Min(0)
    @Max(59)
    private Integer eatTimeMinute;

    private String memo;




}
