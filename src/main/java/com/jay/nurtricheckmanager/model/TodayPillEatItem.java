package com.jay.nurtricheckmanager.model;

import com.jay.nurtricheckmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TodayPillEatItem {
    private String pillFullName;
    private Integer dosesCount;
    private Integer realDosesCount;

    private TodayPillEatItem(){

    }
    public static class TodayPillEatItemBuilder implements CommonModelBuilder<TodayPillEatItem>{
        private final String pillFullName;
        private final Integer dosesCount;
        private final Integer realDosesCount;

        public TodayPillEatItemBuilder(String pillFullName,Integer dosesCount,Integer realDosesCount){
            this.pillFullName = pillFullName;
            this.dosesCount = dosesCount;
            this.realDosesCount = realDosesCount;
        }

        @Override
        public TodayPillEatItem build() {
            return new TodayPillEatItem(this);
        }
    }
}
