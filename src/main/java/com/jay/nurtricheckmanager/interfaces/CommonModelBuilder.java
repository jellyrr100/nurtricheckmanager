package com.jay.nurtricheckmanager.interfaces;


public interface CommonModelBuilder<T> {
    T build();
}


